function openInfo(e){
	var infoWindow = Alloy.createController('info').getView();
	infoWindow.open();
	
}

function openMap(e){
	var mapWindow = Alloy.createController('map').getView();
	mapWindow.open();
}

function openNews(e){
	var newsWindow = Alloy.createController('news').getView();
	newsWindow.open();
}

function openExhibit(e){
	var exhibitWindow = Alloy.createController('exhibit').getView();
	exhibitWindow.open();
}
	
$.index.open();
