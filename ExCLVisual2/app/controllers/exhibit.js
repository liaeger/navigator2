var args = arguments[0] || {};

function openAllComponents(e){
	var allComponentsWindow = Alloy.createController('allComponents').getView();
	allComponentsWindow.open();
}

function openComponent(e){
	var componentWindow = Alloy.createController('component').getView();
	componentWindow.open();
}

$.exhibit.open();
