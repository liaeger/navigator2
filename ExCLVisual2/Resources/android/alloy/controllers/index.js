function Controller() {
    function openInfo() {
        var infoWindow = Alloy.createController("info").getView();
        infoWindow.open();
    }
    function openMap() {
        var mapWindow = Alloy.createController("map").getView();
        mapWindow.open();
    }
    function openNews() {
        var newsWindow = Alloy.createController("news").getView();
        newsWindow.open();
    }
    function openExhibit() {
        var exhibitWindow = Alloy.createController("exhibit").getView();
        exhibitWindow.open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "white",
        backgroundImage: "Nature/1.jpg",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.__alloyId6 = Ti.UI.createView({
        layout: "vertical",
        id: "__alloyId6"
    });
    $.__views.index.add($.__views.__alloyId6);
    $.__views.mapButton = Ti.UI.createButton({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        left: "0",
        bottom: "10",
        title: "Go to Map",
        id: "mapButton"
    });
    $.__views.__alloyId6.add($.__views.mapButton);
    openMap ? $.__views.mapButton.addEventListener("click", openMap) : __defers["$.__views.mapButton!click!openMap"] = true;
    $.__views.newsButton = Ti.UI.createButton({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        left: "0",
        title: "Go to News",
        id: "newsButton"
    });
    $.__views.__alloyId6.add($.__views.newsButton);
    openNews ? $.__views.newsButton.addEventListener("click", openNews) : __defers["$.__views.newsButton!click!openNews"] = true;
    $.__views.infoButton = Ti.UI.createButton({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        left: "0",
        title: "Go to Info",
        id: "infoButton"
    });
    $.__views.__alloyId6.add($.__views.infoButton);
    openInfo ? $.__views.infoButton.addEventListener("click", openInfo) : __defers["$.__views.infoButton!click!openInfo"] = true;
    $.__views.exhibitButton = Ti.UI.createButton({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        left: "0",
        title: "Go to Exhibits",
        id: "exhibitButton"
    });
    $.__views.__alloyId6.add($.__views.exhibitButton);
    openExhibit ? $.__views.exhibitButton.addEventListener("click", openExhibit) : __defers["$.__views.exhibitButton!click!openExhibit"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.index.open();
    __defers["$.__views.mapButton!click!openMap"] && $.__views.mapButton.addEventListener("click", openMap);
    __defers["$.__views.newsButton!click!openNews"] && $.__views.newsButton.addEventListener("click", openNews);
    __defers["$.__views.infoButton!click!openInfo"] && $.__views.infoButton.addEventListener("click", openInfo);
    __defers["$.__views.exhibitButton!click!openExhibit"] && $.__views.exhibitButton.addEventListener("click", openExhibit);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;