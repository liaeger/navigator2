function Controller() {
    function openAllComponents() {
        var allComponentsWindow = Alloy.createController("allComponents").getView();
        allComponentsWindow.open();
    }
    function openComponent() {
        var componentWindow = Alloy.createController("component").getView();
        componentWindow.open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "exhibit";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.exhibit = Ti.UI.createWindow({
        backgroundColor: "white",
        backgroundImage: "Nature/5.jpg",
        id: "exhibit"
    });
    $.__views.exhibit && $.addTopLevelView($.__views.exhibit);
    $.__views.__alloyId4 = Ti.UI.createView({
        layout: "vertical",
        id: "__alloyId4"
    });
    $.__views.exhibit.add($.__views.__alloyId4);
    $.__views.__alloyId5 = Ti.UI.createLabel({
        text: "Welcome to the Exhibit Window",
        id: "__alloyId5"
    });
    $.__views.__alloyId4.add($.__views.__alloyId5);
    $.__views.allComponentsButton = Ti.UI.createButton({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        left: "0",
        title: "Go to All Components",
        id: "allComponentsButton"
    });
    $.__views.__alloyId4.add($.__views.allComponentsButton);
    openAllComponents ? $.__views.allComponentsButton.addEventListener("click", openAllComponents) : __defers["$.__views.allComponentsButton!click!openAllComponents"] = true;
    $.__views.componentButton = Ti.UI.createButton({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        left: "0",
        title: "Go to Component",
        id: "componentButton"
    });
    $.__views.__alloyId4.add($.__views.componentButton);
    openComponent ? $.__views.componentButton.addEventListener("click", openComponent) : __defers["$.__views.componentButton!click!openComponent"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    $.exhibit.open();
    __defers["$.__views.allComponentsButton!click!openAllComponents"] && $.__views.allComponentsButton.addEventListener("click", openAllComponents);
    __defers["$.__views.componentButton!click!openComponent"] && $.__views.componentButton.addEventListener("click", openComponent);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;